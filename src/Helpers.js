global.$ = (selector) => {
	return document.querySelector(selector);
}

global.setClass = (el, classname, bool) => {
	if(bool){
		el.classList.add(classname);
	}else{
		el.classList.remove(classname);
	}
}

global.toggleClass = (el, classname) => {
	let hasClass = el.classList.contains(classname);
	if(hasClass){
		el.classList.remove(classname);
	}else{
		el.classList.add(classname);
	}
	return hasClass;
}

global.radioClass = (classname, selector, selected) => {
	if(window.$(selector+"."+classname) != null){
		window.$(selector+"."+classname).classList.remove(classname);
	}
	console.log(selector+selected);
	window.$(selector+selected).classList.add(classname);
}