import React from 'react';
import './PluginTab.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCog, faTrash, faPlusCircle } from '@fortawesome/free-solid-svg-icons';

class PluginTab extends React.Component {
	constructor(props) {
		super(props);
		this.state = Object.assign(props.data);
		console.log("PLUGIN TAB", this.state);
		this.hiddenFileInput = React.createRef();
		this.onPluginChange = this.onPluginChange.bind(this);
	}

	handleFileClick = e => {
		this.hiddenFileInput.current.click();
	};

	componentDidMount() {

	}

	reloadPlugins = async () => {
		const response = await fetch("/plugins");
		const pluginDataRaw = await response.json();
		this.setState(Object.assign(this.state, { "plugins": pluginDataRaw }));
	}

	installNewPlugin = async (e) => {
		console.log(e);

		var fd = new FormData();
		fd.append('file', e.target.files[0]);
		console.log(e.target.files[0]);
		//return;

		const requestOptions = {
			method: 'POST',
			body: fd
		};
		console.log(requestOptions);
		await fetch('/install_plugin', requestOptions)
			.then(response => response.json());

		this.reloadPlugins();
	}

	pluginSettings = async (e) => {
		console.log(e.target);
		let plugin = e.target.closest(".plugin-entry").querySelector(".plugin-entry-settings");
		console.log(plugin);
		window.toggleClass(plugin, "expanded");
	}

	savePlugin = async (e) => {
		let pluginID = e.target.closest(".plugin-entry").id;
		let pluginSettings = Object.assign(this.state[pluginID].settings);

		console.log(pluginSettings);
		
		const requestOptions = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json', 'Accept': 'application/json' },
			body: JSON.stringify({ "pluginName": pluginID, "settings": pluginSettings })
		}

		let saveStatus = await fetch('/save_plugin', requestOptions)
			.then(response => response.json());
		console.log(saveStatus);
		document.querySelector("#" + pluginID + " .save-status").textContent = "Plugin saved!";
		setTimeout(function () {
			document.querySelector("#" + pluginID + " .save-status").textContent = "";
		}, 3000);
	}

	onPluginChange(e) {
		let pluginName = e.target.closest(".plugin-entry").id;
		let varname = e.target.getAttribute("name");
		let varval = e.target.value;
		console.log(this.state[pluginName].settings[varname])
		if(typeof this.state[pluginName].settings[varname] == "boolean"){
			varval = e.target.checked;
			window.setClass(e.target.parentElement, "checked", varval);
		}

		let thisPlugin = Object.assign(this.state)[pluginName];
		thisPlugin.settings[varname] = varval;
		this.setState({ [pluginName]: thisPlugin });
	}

	deletePlugin = async (e) => {
		let confirmation = window.confirm("Are you sure you want to delete this plugin?");
		if (confirmation == false) { return; }

		let currentPlugins = Object.assign(this.state);

		let pluginID = e.target.closest(".plugin-entry").id;

		const requestOptions = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json', 'Accept': 'application/json' },
			body: JSON.stringify({ "pluginName": pluginID })
		}

		let saveStatus = await fetch('/delete_plugin', requestOptions)
			.then(response => response.json());
		console.log(saveStatus);
		if (saveStatus.status == "SUCCESS") {
			delete currentPlugins[pluginID];
			this.setState(Object.assign(this.state, { "plugins": currentPlugins }));
		}
	}

	renderSettings(settings, sForm) {
		
		let table = [];
		for (let f in sForm) {
			console.log(settings[f]);
			if(settings[f] == null){
				
				settings[f] = sForm[f].defaultvalue;
			}
			switch (sForm[f].type) {
				case 'select':
					let selectOptions = [];
					for (let o in sForm[f].options) {
						selectOptions.push(<option value={o}>{sForm[f].options[o]}</option>);
					}
					table.push(<label>{sForm[f].name}<select id={f + "Value"} name={f} defaultValue={settings[f]} onChange={this.onPluginChange}>{selectOptions}</select></label>);
					break;
				case 'boolean':
					table.push(<div className="boolswitch-div">{sForm[f].name}<label className={settings[f] ? "boolswitch checked" : "boolswitch"}><input type="checkbox" name={f} defaultChecked={settings[f]} onChange={this.onPluginChange} />
						<div></div></label></div>); 
					break;
				case 'text':
					table.push(<label>{sForm[f].name}<input type="text" name={f} defaultValue={settings[f]} onChange={this.onPluginChange} /></label>);
					break;
				case 'number':
					table.push(<label>{sForm[f].name}<input type="number" name={f} defaultValue={settings[f]} onChange={this.onPluginChange} /></label>);
					break;
			}
		}
		table.push(<div className="save-div"><button className="save-button" onClick={this.savePlugin}>Save</button><div className="save-status"></div></div>);
		return table;
	}

	render() {

		let pluginList = [];
		for (let p in this.state) {
			pluginList.push(
				<div className="plugin-entry" id={p}>
					<div className="plugin-entry-ui">
						<img src={this.state[p]['path'] + "/icon.png"} />
						<div className="plugin-entry-info">
							<div className="plugin-entry-title">{p}</div>
							<a href={this.state[p]["path"]} target="_blank">{this.state[p]["path"]}</a>
						</div>
						<FontAwesomeIcon icon={faCog} size="lg" onClick={this.pluginSettings} />
						<FontAwesomeIcon icon={faTrash} size="lg" onClick={this.deletePlugin} />
					</div>
					<div className="plugin-entry-settings">
						{this.renderSettings(this.state[p]['settings'], this.state[p]['settings-form'])}
					</div>
				</div>
			);
		}

		return (<div className="plugin-element">
			<div className="plugin-install-button">
				<label htmlFor='input-file'>
					<button onClick={this.handleFileClick}>Install New Plugin <FontAwesomeIcon icon={faPlusCircle} size="lg" /></button>
				</label>
				<input type='file' id='input-file' ref={this.hiddenFileInput} onChange={this.installNewPlugin} style={{ display: 'none' }} />
			</div>
			<div className="plugin-list">{pluginList}</div></div>);
	}
}

export { PluginTab };