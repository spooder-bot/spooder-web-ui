
import React from 'react';
import ReactDOM from 'react-dom';
import {Monitor} from './Monitor.js';
import {CommandTable} from './CommandTable.js';
import {ConfigTab} from './ConfigTab.js';
import {PluginTab} from './PluginTab.js';
import OSC from 'osc-js';

//import logo from './logo.svg';
const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);

var oscConnected = false;

var username = urlParams.get("user");

var hostIP = window.location.hostname;
var clientIP = null;
var hostPort = 3001;
var osc = null;

if(urlParams.get("host") != null){
	hostIP = urlParams.get("host");
}

class App extends React.Component {

	constructor(props){
		super(props);

		this.getServerState()
		.then((data)=>{
			console.log(data);
			let serverData = data;
			hostPort = serverData.host.port;
			osc = new OSC({plugin: new OSC.WebsocketClientPlugin({host:serverData.osc.host,port:serverData.osc.port,secure:false})});
			this.initOSC();
			this.setState(Object.assign(this.state, {"host":hostPort}));
		})
		.catch(err => console.log(err))
	}
	
	state = {
		data: null
	}
	
	commandRef = React.createRef();
	configRef = React.createRef();
	monitorRef = React.createRef();
	pluginRef = React.createRef();
	
	selectTab = this.selectTab.bind(this);
	setTabContent = this.setTabContent.bind(this);

	componentDidMount(){
		this.setTabContent();
	}
	
	initOSC(){
		console.log("OPENING OSC");
		osc.open();
		osc.on("open", () =>{
			console.log("OSC OPEN");
			osc.send(new OSC.Message('/frontend/connect', 1.0));
		});
		osc.on('*', (message)=>{
			if(message.address.startsWith("/frontend")){
				console.log("I HEARD SOMETHING", message);
				if(this.state.tab == "monitor"){
					
					this.monitorRef.current.addMonitorLog(message);
				}
			}
		});
	}
	
	getServerState = async () => {
		const response = await fetch("/server_state");
		const serverStateRaw = await response.json();
		username = serverStateRaw.user;
		hostPort = serverStateRaw.host.port;
		document.querySelector(".login .account-info").textContent = username;
		return serverStateRaw;
	}
	
	selectTab(e){
		console.log("TAB SELECT", e.target);
		document.querySelector(".tab-button.selected").classList.remove("selected");
		e.target.classList.add("selected");
		this.setTabContent();
	}
	
	setTabContent(){
		console.log("SET TAB CONTENT");
		let tab = document.querySelector(".tab-button.selected").name;
		this.setState(Object.assign(this.state, {"tab":tab}));
		switch(tab){
			case "monitor":
				ReactDOM.render(<Monitor ref={this.monitorRef}/>, document.getElementById("tabContent"));
			break;
			case "commands":
				this.loadCommandData();
			break;
			case "config":
				this.loadConfigData();
			break;
			case "plugins":
				this.loadPlugins();
			break;
		}
	}
	
	loadMonitor = () => {
		ReactDOM.render(<Monitor ref={this.monitorRef}/>, document.getElementById("tabContent"));
	}
	
	loadPlugins = async () => {
		const response = await fetch("/plugins");
		const pluginDataRaw = await response.json();
		//const pluginData = JSON.parse(pluginDataRaw);
		console.log("I GOT PLUGINS! ",pluginDataRaw);

		if(response.status !== 200){
			throw Error("Error: "+response.status);
		}
		ReactDOM.render(<PluginTab ref={this.pluginRef} data={pluginDataRaw} />, document.getElementById("tabContent"));
	}
	
	loadConfigData = async () => {
		const response = await fetch("/server_config");
		const configDataRaw = await response.json();
		const configData = JSON.parse(configDataRaw.express);
		console.log("I GOT CONFIG! ",configData);
		
		var body = configData;

		if(response.status !== 200){
			throw Error(body.message);
		}
		ReactDOM.render(<ConfigTab ref={this.configRef} data={configData} />, document.getElementById("tabContent"));
	}
	
	loadConfigData = this.loadConfigData.bind(this);
	
	
	loadCommandData = async () => {
		const response = await fetch("/command_table");
		const commandDataRaw = await response.json();
		const commandData = JSON.parse(commandDataRaw.express);
		console.log("I GOT EVENTS! ",commandData);
		
		var body = commandData;

		if(response.status !== 200){
			throw Error(body.message);
		}
		ReactDOM.render(<CommandTable ref={this.commandRef} data={commandData} />, document.getElementById("tabContent"));
	}
	
	loadCommandData = this.loadCommandData.bind(this);
	
	render(){
		return(
			<div className="App">
				<div className="App-header">
					<h1 className="App-title">Spooder</h1>
					<div className="login">
						<div className="account-info">{username=="" ? "NOT LOGGED IN":username}</div>
						<div className="login-buttons">
							<a href={"https://id.twitch.tv/oauth2/authorize?client_id=rxaykc294ltsig47tqzegubqrwmhgc&redirect_uri=http://localhost:"+hostPort+"/handle&response_type=code&scope=chat:read chat:edit whispers:read whispers:edit"}>login</a>
							<a href="https://id.twitch.tv/oauth2/revoke?client_id=rxaykc294ltsig47tqzegubqrwmhgc">revoke</a>
						</div>
					</div>
				</div>
				<div className="App-content">
					<div className="navigation-tabs">
						<button type="button" name="monitor" className="tab-button selected" onClick={this.selectTab}>Monitor</button>
						<button type="button" name="commands" className="tab-button" onClick={this.selectTab}>! Commands</button>
						<button type="button" name="plugins" className="tab-button" onClick={this.selectTab}>Plugins</button>
						<button type="button" name="config" className="tab-button" onClick={this.selectTab}>Config</button>
					</div>
					<div id="tabContent">
						
					</div>
				</div>
			</div>
		);
	}
}
export default App;
