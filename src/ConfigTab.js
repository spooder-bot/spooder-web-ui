import React from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faTrash} from '@fortawesome/free-solid-svg-icons';

class ConfigTab extends React.Component{
	constructor(props){
		super(props);
		this.state = Object.assign(props.data);
		console.log(this.state);
		this.handleChange = this.handleChange.bind(this);
		this.toggleSwitch = this.toggleSwitch.bind(this);
		this.handleAddOSCVar = this.handleAddOSCVar.bind(this);
		this.saveConfig = this.saveConfig.bind(this);
		this.addSubVar = this.addSubVar.bind(this);
		this.deleteOSCVar = this.deleteOSCVar.bind(this);
		this.deleteUDPClient = this.deleteUDPClient.bind(this);
		this.getEventSubs();
	}
	
	handleChange(s){
		
		let name = s.target.name;
		let section = s.target.getAttribute("sectionname");
		let newSection = Object.assign(this.state[section]);
		
		if(section != "oscvars"){
			if(s.target.type == "checkbox"){
				this.toggleSwitch(s.target);
				newSection[name] = s.target.checked;
			}else{
				newSection[name] = s.target.value;
			}
		}else{
			newSection[s.target.getAttribute("varname")][name] = s.target.value;
		}
		this.setState(Object.assign(this.state,{[section]:newSection}));
	}
	
	toggleSwitch(e){
		
		console.log("TOGGLE SWITCH");
		
		let thisSwitch = e.closest(".boolswitch");
		
		if(e.checked == false){
			thisSwitch.classList.remove("checked");
		}else{
			thisSwitch.classList.add("checked");
		}
	}
	
	handleAddOSCVar(){
		let name = window.$(".add-osc-var [name='name']").value;
		let handlerFrom = window.$(".add-osc-var [name='handlerFrom']").value;
		let handlerTo = window.$(".add-osc-var [name='handlerTo']").value;
		let addressFrom = window.$(".add-osc-var [name='addressFrom']").value;
		let addressTo = window.$(".add-osc-var [name='addressTo']").value;
		
		let section = "oscvars";
		let newSection = Object.assign(this.state[section]);
		newSection[name] = {
			"handlerFrom":handlerFrom,
			"handlerTo":handlerTo,
			"addressFrom":addressFrom,
			"addressTo":addressTo
		}
		
		this.setState(Object.assign(this.state,{[section]:newSection}));
	}
	
	addSubVar(e){
		console.log(e.target.closest(".config-sub-var.add"));
		let el = e.target.closest(".config-sub-var.add")
		let sectionName = e.target.closest(".config-variable.sub-section").getAttribute("sectionname");
		console.log(sectionName);
		let varname = e.target.closest(".config-variable.sub-section").getAttribute("varname");
		let newUDPClients = Object.assign(this.state[sectionName][varname]);

		let clientKey = el.querySelector(".config-sub-var-ui input[name='clientKey']").value;
		let clientName = el.querySelector(".config-sub-var-ui input[name='clientName']").value;
		let clientIP = el.querySelector(".config-sub-var-ui input[name='clientIP']").value
		let clientPort = el.querySelector(".config-sub-var-ui input[name='clientPort']").value;

		newUDPClients[clientKey] = {
			name:clientName,
			ip:clientIP,
			port:clientPort
		};
		console.log(newUDPClients);
		this.setState(Object.assign(this.state, {"network":Object.assign(this.state.network,{udp_clients:newUDPClients})}));
	}
	
	saveConfig(){
		let newList = Object.assign(this.state);
		let eventSubConfig = {
			"sectionname":this.state.eventsub.sectionname,
			"enabled":this.state.eventsub.enabled,
			"callback_url":this.state.eventsub.callback_url
		}
		newList.eventsub = eventSubConfig;
		
		const requestOptions = {
			method: 'POST',
			headers: {'Content-Type': 'application/json', 'Accept':'application/json'},
			body: JSON.stringify(newList)
		};
		console.log(requestOptions);
		fetch('/saveConfig', requestOptions)
		.then(response => response.json())
		.then(data => {
			if(data.status == "SAVE SUCCESS"){
				document.querySelector("#saveStatusText").textContent = "Config has saved! Restart Spooder to take effect.";
				setTimeout(()=>{
					document.querySelector("#saveStatusText").textContent = "";
				}, 5000)
			}else{
				document.querySelector("#saveStatusText").textContent = "Error: "+data.status;
			}
		});
	}

	deleteUDPClient(e){
		console.log(e.currentTarget);
		let el = e.currentTarget.closest(".config-sub-var");
		let sectionname = el.getAttribute("sectionname");
		let varname = el.getAttribute("varname");
		let subvarname = el.getAttribute("subvarname");

		let newUDPClients = Object.assign(this.state[sectionname][varname]);
		delete newUDPClients[subvarname];
		
		this.setState(Object.assign(this.state, {"network":Object.assign(this.state.network,{udp_clients:newUDPClients})}));

	}

	deleteOSCVar(e){
		console.log(e.currentTarget);
		let el = e.currentTarget.closest(".config-variable");

		let varname = el.getAttribute("varname");

		let oscVars = Object.assign(this.state["oscvars"]);
		delete oscVars[varname];

		this.setState(Object.assign(this.state, {"oscvars":oscVars}));
	}

	getEventSubs = async() => {

		let eventsubsRaw = await fetch('/get_eventsub')
		.then(response => response.json());
		let eventsubs = eventsubsRaw.data;
		
		let newEventState = {
			"sectionname":this.state.eventsub.sectionname,
			"enabled":this.state.eventsub.enabled,
			"callback_url":this.state.eventsub.callback_url
		}
		console.log(newEventState);
		for(let e in eventsubs){
			if(newEventState[eventsubs[e].type] == null){
				newEventState[eventsubs[e].type] = [];
			}

			newEventState[eventsubs[e].type].push(eventsubs[e]);
			
		}
		

		this.setState(Object.assign(this.state, {"eventsub":newEventState}));
		console.log(this.state);
	}

	initEventSub = async(e) => {
		let eventType = document.querySelector("#event-sub-add select").value;
		if(eventType == null){return;}
		let eventSub = await fetch('/init_followsub?type='+eventType)
		.then(response => response.json());
		console.log(eventSub);
		document.querySelector("#eventSubAddStatus").textContent = eventSub.status;
		setTimeout(function(){
			document.querySelector("#eventSubAddStatus").textContent = "";
		}, 3000);
		this.getEventSubs();
	}

	deleteEventSub = async(e) => {
		e.preventDefault();
		let followSub = await fetch('/delete_eventsub?id='+e.target.getAttribute("subid"))
		.then(response => response.json());
		console.log(followSub);

		this.getEventSubs();
	}
	
	
	render(){
		console.log(this.state);
		let sections = [];
		let table = [];

		let udpTrashButton = <FontAwesomeIcon icon={faTrash} size="lg" onClick={this.deleteUDPClient} />;
		let oscTrashButton = <FontAwesomeIcon icon={faTrash} size="lg" onClick={this.deleteOSCVar} />;

		let udpClients = this.state.network.udp_clients;
		let clientTable = [];

		for(let u in udpClients){
			clientTable.push(<option value={u}>{udpClients[u].name}</option>);
		}
		
		for(let s in this.state){
			table = [];
			//console.log(s);
			for(let ss in this.state[s]){
				if(ss=="sectionname"){continue;}
				let dataType = typeof this.state[s][ss];

				if(s == "eventsub"){
					switch(ss){
						case 'callback_url':
							table.push(<div className="config-variable"><label>{ss}<input type="text" name={ss} sectionname={s} defaultValue={this.state[s][ss]} onChange={this.handleChange} /></label></div>);
						break;
						case 'enabled':
							
							table.push(<div className="config-variable"><label>{ss}</label>
							<label className={this.state[s][ss]?"boolswitch checked":"boolswitch"}><input type="checkbox" name={ss} sectionname={s} defaultChecked={this.state[s][ss]} onChange={this.handleChange}/>
							<div></div></label></div>);
							break;
						default:
							let subTable = [];
							let eventSubs = Object.assign(this.state.eventsub[ss]);
							for(let f in eventSubs){
								subTable.push(
									<div><label>ID: {eventSubs[f].id}</label><button type="button" className="event-sub-delete-button" onClick={this.deleteEventSub} subid={eventSubs[f].id}>DELETE</button></div>
								);
							}
							table.push(<div className="config-variable"><label>{ss}</label><div>{subTable}</div></div>);
						break;
					}
				}else if(s == "oscvars"){

					table.push(<div className="config-variable" varname={ss}>
								<div className="config-variable-buttons">{oscTrashButton}</div>
								<div className="config-variable-ui">
									<label>{ss}</label>
										<label>Handler From
											<select name="handlerFrom" varname={ss} sectionname={s} defaultValue={this.state[s][ss]["handlerFrom"]} onChange={this.handleChange}>
												<option value="tcp">TCP (Overlays)</option>
												<option value="udp">UDP Any</option>
												{clientTable}
											</select>
										</label>
										<label>Handler To
											<select name="handlerTo" varname={ss} sectionname={s} defaultValue={this.state[s][ss]["handlerTo"]} onChange={this.handleChange}>
												<option value="tcp">TCP (Overlays)</option>
												<option value="udp">UDP All</option>
												{clientTable}
											</select>
										</label>
										<label>Address From:
											<input name="addressFrom" type="text" varname={ss} sectionname={s}  defaultValue={this.state[s][ss]["addressFrom"]} placeholder="OSC Address From" onChange={this.handleChange} />
										</label>
										<label>AddressTo:
											<input name="addressTo" type="text" varname={ss} sectionname={s}  defaultValue={this.state[s][ss]["addressTo"]} placeholder="OSC Address To" onChange={this.handleChange} />
										</label>
									</div>
							</div>)
				}else{
				
					switch(dataType){
						case 'number':
						case 'string':
							table.push(<div className="config-variable"><label>{ss}<input type="text" name={ss} sectionname={s} defaultValue={this.state[s][ss]} onChange={this.handleChange} /></label></div>);
						break;
						case 'boolean':
							
							table.push(<div className="config-variable"><label>{ss}</label>
							<label className={this.state[s][ss]?"boolswitch checked":"boolswitch"}><input type="checkbox" name={ss} sectionname={s} defaultChecked={this.state[s][ss]} onChange={this.handleChange}/>
							<div></div></label></div>);
							break;
						case 'object':
							let subTable = [];
							//console.log(this.state);
							for(let c in this.state[s][ss]){
								
								subTable.push(<div className="config-sub-var" sectionname={s} varname={ss} subvarname={c}>
									<div className="config-sub-var-buttons">
										{udpTrashButton}
									</div>
									<div className="config-sub-var-ui">
										<label>
											{c}
										</label>
										<label>Name:
											<input name="clientName" type="text" defaultValue={this.state[s][ss][c]['name']} placeholder="Name of client" onChange={this.handleChange} />
										</label>
										<label>IP:
											<input name="clientIP" type="text" defaultValue={this.state[s][ss][c]['ip']} placeholder="IP address to send to" onChange={this.handleChange} />
										</label>
										<label>Port:
											<input name="clientPort" type="text" defaultValue={this.state[s][ss][c]['port']} placeholder="IP port to send to" onChange={this.handleChange} />
										</label>
									</div>
								</div>);
							}
							let addSubVarForm = <div className="config-sub-var add">
													
													<div className="config-sub-var-ui">
														<label>Key:
															<input name="clientKey" type="text" placeholder="Key name for storage"  />
														</label>
														<label>Name:
															<input name="clientName" type="text" placeholder="Name of client"  />
														</label>
														<label>IP:
															<input name="clientIP" type="text" placeholder="IP address to send to"  />
														</label>
														<label>Port:
															<input name="clientPort" type="text" placeholder="IP port to send to"  />
														</label>
													</div>
													<div className="config-sub-var-buttons">
														<button type="button" className="add-button" onClick={this.addSubVar}>Add</button>
													</div>
												</div>
							table.push(<div varname={ss} sectionname={s} className="config-variable sub-section"><label>{ss} {subTable}
							<div className="config-variable add">{addSubVarForm}</div></label>
							</div>);
							
						break;
					}
				}
			}
			if(s == "eventsub"){
				table.push(<div id="event-sub-add">
						<select>
							<option value="channel.update">Channel Update</option>
							<option value="channel.follow">Follow</option>
							{/*<option value="channel.subscribe">Subscribe</option>
							<option value="channel.subscription.end">Subscription End</option>
							<option value="channel.subscription.gift">Subscription Gift</option>
							<option value="channel.subscription.message">Subscription Message</option>
							<option value="channel.cheer">Cheer</option>
							<option value="channel.raid">Raid</option>
							<option value="channel.ban">Ban</option>
							<option value="channel.unban">Unban</option>
							<option value="channel.moderator.add">Mod Add</option>
							<option value="channel.moderator.remove">Mod Remove</option>
							<option value="channel.channel_points_custom_reward.add">Channel Points Custom Reward Add</option>
							<option value="channel.channel_points_custom_reward.update">Channel Points Custom Reward Update</option>
							<option value="channel.channel_points_custom_reward.remove">Channel Points Custom Reward Remove</option>
							<option value="channel.channel_points_custom_reward_redemption.add">Channel Points Custom Reward Redemption Add</option>
							<option value="channel.channel_points_custom_reward_redemption.update">Channel Points Custom Reward Redemption Update</option>
							<option value="channel.poll.begin">Poll Begin</option>
							<option value="channel.poll.progress">Poll Progress</option>
							<option value="channel.poll.end">Poll End</option>
							<option value="channel.prediction.begin">Prediction Begin</option>
							<option value="channel.prediction.progress">Prediction Progress</option>
							<option value="channel.prediction.lock">Prediction Lock</option>
							<option value="channel.prediction.end">Prediction End</option>
							<option value="drop.entitlement.grant">Drop Entitlement Grant</option>
							<option value="extension.bits_transaction.create">Extension Bits Transaction Create</option>
							<option value="channel.goal.begin">Goal Begin</option>
							<option value="channel.goal.progress">Goal Progress</option>
							<option value="channel.goal.end">Goal End</option>
							<option value="channel.hype_train.begin">Hype Train Begin</option>
							<option value="channel.hype_train.progress">Hype Train Progress</option>
							<option value="channel.hype_train.end">Hype Train End</option>
							<option value="stream.online">Stream Online</option>
							<option value="stream.offline">Stream Offline</option>
							<option value="user.authorization.grant">User Authorization Grant</option>
							<option value="user.authorization.revoke">User Authorization Revoke</option>
							<option value="user.update">User Update</option>*/}
						</select><button type="button" onClick={this.initEventSub}>Add</button><div id="eventSubAddStatus"></div></div>);
			}
			if(s == "oscvars"){
				table.push(<div className="add-osc-var">
				<div className="config-variable">
					<label>Name:<input type="text" name="name" placeholder="Name" /></label>
					<label>Handler From
						<select defaultValue="tcp" name="handlerFrom">
							<option value="tcp">TCP (Overlays)</option>
							<option value="udp">UDP Any</option>
							{clientTable}
						</select>
					</label>
					<label>Handler To
						<select defaultValue="tcp" name="handlerTo">
							<option value="tcp">TCP (Overlays)</option>
							<option value="udp">UDP All</option>
							{clientTable}
						</select>
					</label>
					<label>Address From:<input type="text" name="addressFrom" placeholder="OSC Address From" /></label>
					<label>Address To:<input type="text" name="addressTo" placeholder="OSC Address To" /></label>
				</div>
				<button type="button" className="add-button" onClick={this.handleAddOSCVar}>Add</button></div>);
			}
			sections.push(<div className="config-element" name={s}><label>{this.state[s]["sectionname"]}</label>{table}</div>);
		}
		
		return (
			<form className="config-tab">
				{sections}
				<div className="save-commands"><button type="button" id="saveCommandsButton" className="save-button" onClick={this.saveConfig}>Save</button><div id="saveStatusText" className="save-status"></div></div>
			</form>
		);
	}
}

export {ConfigTab};


