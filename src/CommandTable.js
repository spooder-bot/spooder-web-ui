import React from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faTrash} from '@fortawesome/free-solid-svg-icons';

class CommandTable extends React.Component{
	constructor(props){
		super(props);
		this.state = Object.assign(props.data);
		this.handleChange = this.handleChange.bind(this);
		this.addCommand = this.addCommand.bind(this);
		this.saveCommands = this.saveCommands.bind(this);
		this.deleteCommand = this.deleteCommand.bind(this);
		console.log(this.state);
		if(Object.keys(this.state.udpClients) == 0){
			alert("No UDP Clients found. You can set these up in the Config tab.")
		}
	}
	
	handleChange(s){
		let newCommand = Object.assign(this.state.commands[s.target.dataset.key]);
		newCommand[s.target.name] = s.target.value;
		this.setState(Object.assign(this.state, {"commands":Object.assign(this.state.commands,{[s.target.dataset.key]:newCommand})}));
	}
	
	addCommand(e){
		let commandKey = document.querySelector("[name=key]").value;
		if(commandKey === ""){
			alert("The command is blank");
			return;
		}
		if(Object.keys(this.state).includes(commandKey)){
			alert("That command is already used");
			return;
		}
		
		let commandType = document.querySelector(".add-command [name=type]").value;
		let newCommand = {
			name: document.querySelector(".add-command [name=name]").value,
			type: commandType
		};
		switch(commandType){
			case "response":
				newCommand.message = document.querySelector("#addResponseCommand [name=message]").value;
				document.querySelector("#addResponseCommand [name=message]").value = "";
			break;
			case "event":
				newCommand.address = document.querySelector("#addEventCommand [name=address]").value;
				newCommand.valueOn = document.querySelector("#addEventCommand [name=valueOn]").value;
				newCommand.valueOff = document.querySelector("#addEventCommand [name=valueOff]").value;
				newCommand.etype = document.querySelector("#addEventCommand [name=etype]").value;
				newCommand.duration = document.querySelector("#addEventCommand [name=duration]").value;
				newCommand.dest_udp = document.querySelector("#addEventCommand [name=dest_udp]").value;
				newCommand.description = document.querySelector("#addEventCommand [name=description]").value;

				document.querySelector("#addEventCommand [name=address]").value = "";
				document.querySelector("#addEventCommand [name=valueOn]").value = "";
				document.querySelector("#addEventCommand [name=valueOff]").value = "";
				document.querySelector("#addEventCommand [name=etype]").value = "timed";
				document.querySelector("#addEventCommand [name=duration]").value = 300;
				document.querySelector("#addEventCommand [name=dest_udp]").value = -1;
				document.querySelector("#addEventCommand [name=description]").value = "";

			break;
			default:
				console.log("No command type.");
		}
		console.log("NEW COMMAND", commandKey, newCommand);
		this.setState(Object.assign(this.state.commands, {[commandKey]:newCommand}));
	}
	
	saveCommands(){
		let newList = Object.assign(this.state.commands);
		for(let c in newList){
			for(let n in newList[c]){
				if(!isNaN(newList[c][n])){
					newList[c][n] = parseFloat(newList[c][n]);
				}
			}
		}
		
		const requestOptions = {
			method: 'POST',
			headers: {'Content-Type': 'application/json', 'Accept':'application/json'},
			body: JSON.stringify(newList)
		};
		console.log(requestOptions);
		fetch('/saveCommandList', requestOptions)
		.then(response => response.json())
		.then(data => {
			if(data.status == "SAVE SUCCESS"){
				document.querySelector("#saveStatusText").textContent = "Commands are saved!";
				setTimeout(()=>{
					document.querySelector("#saveStatusText").textContent = "";
				}, 5000)
			}else{
				document.querySelector("#saveStatusText").textContent = "Error: "+data.status;
			}
		});
	}
	
	deleteCommand(e){
		
		let el = e.currentTarget.closest(".command-element");
		let cKey = el.getAttribute("commandkey");

		let newCommands = Object.assign(this.state.commands);
		delete newCommands[cKey];

		this.setState(Object.assign(this.state, {"commands":newCommands}));
	}
	
	toggleProps(e){
		let topElement = e.currentTarget.closest(".command-element");
		let element = topElement.querySelector(".command-fields");
		
		console.log(element, topElement, e.target);
		
		if(topElement.classList.contains("expanded")){
			topElement.classList.remove("expanded");
		}else{
			topElement.classList.add("expanded");
		}
		
		if(element.classList.contains("hidden")){
			element.classList.remove("hidden");
		}else{
			element.classList.add("hidden");
		}
	}
	
	changeAddType(e){
		document.querySelector(".add-command-form.selected").classList.remove("selected");
		document.querySelector(".add-command-form."+e.target.value).classList.add("selected");
	}

	toggleAddMenu(e){
		let element = document.querySelector(".add-command")
		
		if(element.classList.contains("hidden")){
			element.classList.remove("hidden");
		}else{
			element.classList.add("hidden");
		}
		
		let button = e.target;
		if(button.classList.contains("opened")){
			button.classList.remove("opened");
		}else{
			button.classList.add("opened");
		}
	}
	
	sortList() {
	  var list, i, switching, b, shouldSwitch;
	  list = document.getElementById("id01");
	  switching = true;
	  /* Make a loop that will continue until
	  no switching has been done: */
	  while (switching) {
		// Start by saying: no switching is done:
		switching = false;
		b = list.getElementsByTagName("LI");
		// Loop through all list items:
		for (i = 0; i < (b.length - 1); i++) {
		  // Start by saying there should be no switching:
		  shouldSwitch = false;
		  /* Check if the next item should
		  switch place with the current item: */
		  if (b[i].innerHTML.toLowerCase() > b[i + 1].innerHTML.toLowerCase()) {
			/* If next item is alphabetically lower than current item,
			mark as a switch and break the loop: */
			shouldSwitch = true;
			break;
		  }
		}
		if (shouldSwitch) {
		  /* If a switch has been marked, make the switch
		  and mark the switch as done: */
		  b[i].parentNode.insertBefore(b[i + 1], b[i]);
		  switching = true;
		}
	  }
	}
	
	render(){
		
		let udpHostOptions = [];
		console.log(this.state.udpClients);
		if(Object.keys(this.state.udpClients).length > 0){
			for(let u in this.state.udpClients){
				udpHostOptions.push(
					<option value={u}>{this.state.udpClients[u].name}</option>
				)
			}
		}
		
		let tables = {};
		let trashButton = <FontAwesomeIcon icon={faTrash} size="lg" onClick={this.deleteCommand} />;
		for(let s in this.state.commands){
			
			if(tables[this.state.commands[s].type] == null){
				tables[this.state.commands[s].type] = [];
			}
			var thisTable = tables[this.state.commands[s].type];

			switch(this.state.commands[s].type){
				case 'response':
					thisTable.push(
						<div className="command-element response" key={"command-"+thisTable.length} commandkey={s}>
							<div className="command-key" onClick={this.toggleProps}>
								<label>
									<h1>!{s}</h1>
								</label>
							</div>
							<div className="command-fields hidden">
								<div className="command-actions">
									{trashButton}
								</div>
								<div className="command-props">
									<label>
										Name:
										<input type="text" name="name" data-key={s} defaultValue={this.state.commands[s].name} onChange={this.handleChange} />
									</label>
									<label>
										Response Type:
										<select name="rtype" data-key={s} defaultValue={this.state.commands[s].rtype} onChange={this.handleChange}><option value="command">Command</option><option value="search">Search</option></select>
									</label>
									<label>
										Message:
										<textarea name="message" data-key={s} defaultValue={this.state.commands[s].message} onChange={this.handleChange}></textarea>
									</label>
									<label>
										Description:
										<input type="text" name="description" data-key={s} defaultValue={this.state.commands[s].description} onChange={this.handleChange} />
									</label>
								</div>
							</div>
						</div>
					);
				break;
				case 'event':
					thisTable.push(
						
						<div className="command-element event" key={"command-"+thisTable.length} commandkey={s}>
							<div className="command-key" onClick={this.toggleProps}>
								<label>
									!{s}
								</label>
							</div>
							<div className="command-fields hidden">
								
								<div className="command-actions">
									{trashButton}
								</div>
								<div className="command-props">
									<label>
										Name:
										<input type="text" name="name" data-key={s} defaultValue={this.state.commands[s].name} onChange={this.handleChange} />
									</label>
									
									<label>
										Address:
										<input type="text" name="address" data-key={s} defaultValue={this.state.commands[s].address} onChange={this.handleChange} />
									</label>
									<label>
										UDP:
										<select name="dest_udp" data-key={s} defaultValue={this.state.commands[s].dest_udp} onChange={this.handleChange}>
											<option value={-1}>None</option>
											<option value={-2}>All</option>
												{udpHostOptions}
										</select>
									</label>
									<label>
										Value On:
										<input type="text" name="valueOn" data-key={s} defaultValue={this.state.commands[s].valueOn} onChange={this.handleChange} />
									</label>
									<label>
										Value Off:
										<input type="text" name="valueOff" data-key={s} defaultValue={this.state.commands[s].valueOff} onChange={this.handleChange} />
									</label>
									<label>
										Event Type:
										<select name="etype" data-key={s} defaultValue={this.state.commands[s].etype} onChange={this.handleChange}><option value="timed">Timed</option><option value="oneshot">One Shot</option></select>
									</label>
									<label>
										Duration/Cooldown (Seconds):
										<input type="number" name="duration" data-key={s} defaultValue={this.state.commands[s].duration} onChange={this.handleChange} />
									</label>
									<label>
										Description:
										<input type="text" name="description" data-key={s} defaultValue={this.state.commands[s].description} onChange={this.handleChange} />
									</label>
								</div>
							</div>
						</div>
					);
				break;
				default:
					console.log("Command type not recognized :(");
			}
		}

		let table = [];
		for(let t in tables){
			table.push(
				<div className="command-category">
					{t}
					{tables[t]}
				</div>
			);
		}
		return (
			<form className="command-table">
				<div className="command-container">
					{table}
				</div>
				<button type="button" id="toggleAddCommandButton" className="command-button" onClick={this.toggleAddMenu}>+</button>
				<div className="add-command hidden">
					<div className="add-command-fields">
						<label>
						Command Type:
						<select id="addCommandType" name="type" onChange={this.changeAddType}>
							<option value={"response"}>Reponse</option>
							<option value={"event"}>Event</option>
						</select>
						</label>
						<label>
							! Key:
							<input name="key" type="text" placeholder="! is already included" />
						</label>
						<label>
							Name:
							<input name="name" type="text" placeholder="Name in chat" />
						</label>
						<div id={"addResponseCommand"} className={"add-command-form response selected"}>
							<label>
								Script:
								<textarea name="message" placeholder="Write your response script in JS here. You can access the sender name and tags with 'vars.sender and vars.senderTags' be sure to end the script with 'return <string>' without quotes." ></textarea>
							</label>
						</div>
						<div id={"addEventCommand"} className={"add-command-form event"}>
							<label>
								UPP Destination:
								<select name="dest_udp" defaultValue={-2}>
									<option value={-1}>None</option>
									<option value={-2}>All</option>
										{udpHostOptions}
								</select>
							</label>
							<label>
								Address:
								<input name="address" type="text" placeholder="/something/somethinginside" />
							</label>
							<label>
								Value On:
								<input name="valueOn" type="text" placeholder="Value when activated" />
							</label>
							<label>
								Value Off:
								<input name="valueOff" type="text" placeholder="Value when deactivated" />
							</label>
							<label>
								Event Type:
								<select name="etype" ><option value="timed">Timed</option><option value="oneshot">One Shot</option></select>
							</label>
							<label>
								Duration:
								<input name="duration" type="number" placeholder="Number in seconds" />
							</label>
							<label>
								Description:
								<input type="text" name="description" />
							</label>
						</div>
					</div>
					<div className="add-command-actions">
						<button type="button" id="addCommandButton" className="add-button" onClick={this.addCommand}>Add</button>
					</div>
				</div>
				<div className="save-commands"><button type="button" id="saveCommandsButton" className="save-button" onClick={this.saveCommands}>Save</button><div id="saveStatusText" className="save-status"></div></div>
			</form>
		);
	}
}

export {CommandTable};


