# Spooder Web Source Code

## Develop

Add these folders to the root of your Spooder installation and start Spooder in dev mode (Web UI will use port 3001 instead of the port in the config)

```npm run dev```

Start the frontend development server on another shell.

```npm run start-front```

## Build

Build the web UI

```npm run build-front```

